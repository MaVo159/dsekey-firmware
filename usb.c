#include "chopstx_include.h"

typedef struct usb_dev usb_device_t;
#define VCOM_FEATURE_BUS_POWERED 0x80

typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t bcdUSB;
  uint8_t bDeviceClass;
  uint8_t bDeviceSubClass;
  uint8_t bDeviceProtocol;
  uint8_t bMaxPacketSize;
  uint16_t idVendor;
  uint16_t idProduct;
  uint16_t bcdDevice;
  uint8_t iManufacturer;
  uint8_t iProduct;
  uint8_t iSerialNumber;
  uint8_t bNumConfigurations;
} __attribute__((packed)) usb_device_descriptor_t;

typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint8_t bEndpointAdress;
  uint8_t bmAttributes;
  uint16_t wMaxPacketSize;
  uint8_t bInterval;
} __attribute__((packed)) usb_endpoint_descriptor_t;

typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint8_t bInterfaceNumber;
  uint8_t bAlternateSetting;
  uint8_t bNumEndpoints;
  uint8_t bInterfaceClass;
  uint8_t bInterfaceSubClass;
  uint8_t bInterfaceProtocol;
  uint8_t iInterface;
} __attribute__((packed)) usb_interface_descriptor_t;

typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t bcdHID;
  uint8_t bCountryCode;
  uint8_t bNumReportDescriptors;
  uint8_t bReportDescriptorType;
  uint16_t wReportDescriptorLength;
} __attribute__((packed)) usb_hid_descriptor_t;

typedef struct {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t wTotalLength;
  uint8_t bNumInterfaces;
  uint8_t bConfigurationValue;
  uint8_t iConfiguration;
  uint8_t bmAttributes;
  uint8_t bMaxPower;
} __attribute__((packed)) usb_configuration_descriptor_header_t;

static usb_device_descriptor_t device_desc = {
    .bLength = 18,
    .bDescriptorType = DEVICE_DESCRIPTOR,
    .bcdUSB = 0x0110,
    .bDeviceClass = 0x00,
    .bDeviceSubClass = 0x00,
    .bDeviceProtocol = 0x00,
    .bMaxPacketSize = 0x40,
    .idVendor = 0xFFFF,
    .idProduct = 0x0001,
    .bcdDevice = 0x0100,
    .iManufacturer = 0x04,
    .iProduct = 0x0E,
    .iSerialNumber = 0x30,
    .bNumConfigurations = 1,
};

static uint8_t report_desc[34] = {
    0x06, 0xd0, 0xf1, // USAGE_PAGE (FIDO Alliance)
    0x09, 0x01,       // USAGE (U2F HID Authenticator Device)
    0xa1, 0x01,       // COLLECTION (Application)
    0x09, 0x20,       //   USAGE (Input Report Data)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00, //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x40,       //   REPORT_COUNT (64)
    0x81, 0x02,       //   INPUT (Data,Var,Abs)
    0x09, 0x21,       //   USAGE (Output Report Data)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00, //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x40,       //   REPORT_COUNT (64)
    0x91, 0x02,       //   OUTPUT (Data,Var,Abs)
    0xc0              // END_COLLECTION
};

static struct {
  usb_configuration_descriptor_header_t configuration;
  usb_interface_descriptor_t interface;
  usb_hid_descriptor_t hid;
  usb_endpoint_descriptor_t endpoints[2];
} __attribute__((packed)) config_desc = {
    .configuration =
        {
            .bLength = 9,
            .bDescriptorType = CONFIG_DESCRIPTOR,
            .wTotalLength = 9 + 9 + 9 + 7 + 7,
            .bNumInterfaces = 1,
            .bConfigurationValue = 1,
            .iConfiguration = 0,
            .bmAttributes = VCOM_FEATURE_BUS_POWERED,
            .bMaxPower = 50,
        },
    .interface =
        {
            .bLength = 9,
            .bDescriptorType = 0x04,
            .bInterfaceNumber = 0,
            .bAlternateSetting = 0,
            .bNumEndpoints = 2,
            .bInterfaceClass = 0x03,
            .bInterfaceSubClass = 0x00,
            .bInterfaceProtocol = 0x00,
            .iInterface = 0,
        },
    .hid =
        {
            .bLength = 9,
            .bDescriptorType = 0x21,
            .bcdHID = 0x110,
            .bCountryCode = 0x0,
            .bNumReportDescriptors = 2,
            .bReportDescriptorType = 0x22,
            .wReportDescriptorLength = 0,
        },
    .endpoints =
        {
            {
                .bLength = 7,
                .bDescriptorType = ENDPOINT_DESCRIPTOR,
                .bEndpointAdress = 0x01,
                .bmAttributes = 0x03,
                .wMaxPacketSize = 64,
                .bInterval = 5,
            },
            {
                .bLength = 7,
                .bDescriptorType = ENDPOINT_DESCRIPTOR,
                .bEndpointAdress = 0x81,
                .bmAttributes = 0x03,
                .wMaxPacketSize = 64,
                .bInterval = 5,
            },
        },
};

static void blink(uint8_t n) {
  for (uint8_t i = 0; i < n; i++) {
    set_led(1);
    chopstx_usec_wait(1000);
    set_led(0);
    chopstx_usec_wait(300 * 1000);
  }
  chopstx_usec_wait(700 * 1000);
}

static void get_descriptor(usb_device_t *dev) {
  uint8_t desc_type = dev->dev_req.value >> 8;
  uint8_t string_desc[2] = {2, STRING_DESCRIPTOR};
  switch (desc_type) {
  case DEVICE_DESCRIPTOR:
    usb_lld_ctrl_send(dev, &device_desc, sizeof(device_desc));
    break;
  case CONFIG_DESCRIPTOR:
    usb_lld_ctrl_send(dev, &config_desc, sizeof(config_desc));
    break;
  case STRING_DESCRIPTOR:
    usb_lld_ctrl_send(dev, &string_desc, sizeof(string_desc));
    break;
  }
}

static chopstx_intr_t usb_intr;

void init_usb(void) {
  (void)report_desc;

  usb_device_t dev;
  chopstx_claim_irq(&usb_intr, 24);
  usb_lld_init(&dev, VCOM_FEATURE_BUS_POWERED);

  while (1) {
    chopstx_intr_wait(&usb_intr);
    if (usb_intr.ready) {
      int event = usb_lld_event_handler(&dev);
      uint8_t endpoint = USB_EVENT_ENDP(event);
      uint8_t event_id = USB_EVENT_ID(event);
      if (endpoint == 0) {
        switch (event_id) {
        case USB_EVENT_DEVICE_RESET:
          usb_lld_reset(&dev, VCOM_FEATURE_BUS_POWERED);
          usb_lld_setup_endp(&dev, ENDP0, 1, 1);
          continue;
        case USB_EVENT_GET_DESCRIPTOR:
          get_descriptor(&dev);
          continue;
        case USB_EVENT_SET_CONFIGURATION:
          usb_lld_set_configuration(&dev,dev.dev_req.value);
          usb_lld_ctrl_ack (&dev);
          continue;
        case USB_EVENT_OK:
        case USB_EVENT_DEVICE_ADDRESSED:
          continue;
        case 13:
          blink(2);
          continue;        
        default:
          blink(event_id);
        }
      }
    }
  }
}
