#ifndef INCLUDE_CHAN_H
#define INCLUDE_CHAN_H

#include "chopstx_include.h"

typedef struct {
  chopstx_mutex_t data_lock, transfer_ready, transfer_done;
  void *data;
} chan_t;

void chan_init(chan_t *chan) {
  chopstx_mutex_init(&chan->data_lock);
  chopstx_mutex_init(&chan->transfer_ready);
  chopstx_mutex_lock(&chan->transfer_ready);
  chopstx_mutex_init(&chan->transfer_done);
  chopstx_mutex_lock(&chan->transfer_done);
  chan->data = 0;
}

void chan_send(chan_t *chan, void *data) {
  chopstx_mutex_lock(&chan->data_lock);
  chan->data = data;
  chopstx_mutex_unlock(&chan->transfer_ready);
  chopstx_mutex_lock(&chan->transfer_done);
  chan->data = 0;
  chopstx_mutex_unlock(&chan->data_lock);
}

void *chan_receive_lock(chan_t *chan) {
  chopstx_mutex_lock(&chan->transfer_ready);
  return chan->data;
}

void chan_receive_unlock(chan_t *chan) {
  chopstx_mutex_unlock(&chan->transfer_done);
}

void *chan_receive(chan_t *chan) {
  void *data = chan_receive_lock(chan);
  chan_receive_unlock(chan);
  return data;
}

#endif