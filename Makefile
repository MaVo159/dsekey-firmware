# Makefile for example application of Chopstx

PROJECT = firmware

### Currently, it's for FS-BB48.

CHOPSTX = chopstx
LDSCRIPT= link.ld
CSRC = $(wildcard *.c)
CHIP=mkl27z

USE_SYS = yes
USE_USB = yes
USE_ADC = yes

###################################
CROSS = arm-none-eabi-
CC   = $(CROSS)gcc
LD   = $(CROSS)gcc
OBJCOPY   = $(CROSS)objcopy

MCU   = cortex-m0plus
CWARN = -Wall -Wextra -Wstrict-prototypes -Werror -fmax-errors=3
DEFS  = -DFREE_STANDING -DMHZ=48 -DUSE_SYS3
OPT   = -O3 -Os -g -std=gnu11
LIBS  =

####################
include $(CHOPSTX)/rules.mk

.PHONY: flash
flash: build/$(PROJECT).elf
	openocd -f interface/cmsis-dap.cfg -f target/klx.cfg -c "program build/$(PROJECT).elf verify reset exit"
	
.PHONY: format
fmt:
	clang-format -i *.c *.h